= Check Config Extension
Jonah Wille <jonahtec2003@gmail.com>

ifdef::embedded[]
{author} - {email}
endif::[]

== Typical output

=== Registration

Typical Playbook entry:
-------
- require: "./lib/check-config-extension/lib/index.js"
-------

When the log level is set to `info` or below, then the extension will output: `+INFO (check-config-extension): registered+`

=== Errors

If you get this warning, there is an branch that is named different than the version in the `+antora.yml+` file.

----
WARN (check-config-extension): unequal version to branch name found in AAA: BBB != CCC
----

* AAA: the *repository* where the error is
* BBB: the version in the `+antora.yml+` file
* CCC: a list of branch and tag names specified in the *antora playbook*

=== Debug

When the log level is set to `debug` the extension will output the list of branches and tags found in the playbook

==== Replacing tags for testing

-------
- require: "./lib/check-config-extension/lib/index.js"
      data:
        replace:
        - this: 'other-tag'
          that: 'newer-tag'
-------
This entry will replace "other-tag" with "newer-tag", this should not be used in production!