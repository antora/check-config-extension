'use strict'
// The name of the package in order to give the Antora logger a useful name
const { name: packageName } = require('../package.json')

var logger

module.exports.register = function ({ config }) {
    logger = this.require('@antora/logger').get(packageName)

    logger.info("registered")

    this.on('contentAggregated', function ({ playbook, contentAggregate }) {

        var sources = new Array

        for (let i = 0; i < playbook.content.sources.length; i++) {
            const source = playbook.content.sources[i];
            var versions = new Array
            if (source.tags != undefined) {
                if (Array.isArray(source.tags)) {
                    for (let ii = source.tags.length; ii > 0; ii--) {
                        if (source.tags[ii - 1] != undefined) {
                            versions.push(source.tags[ii - 1])
                        }
                    }
                } else {
                    versions.push(source.tags)
                }
            }
            if (source.branches != undefined) {
                if (Array.isArray(source.branches)) {
                    for (let ii = source.branches.length; ii > 0; ii--) {
                        if (source.branches[ii - 1] != undefined) {
                            versions.push(source.branches[ii - 1])
                        }
                    }
                } else {
                    versions.push(source.branches)
                }
            }
            sources.push(versions)
        }

        log_arr(sources)

        var lastname = ""
        var vernr = 0

        contentAggregate.forEach(element => {

            try {
                if (config.data.replace !== undefined) {
                    var log = "replaced " + element.version
                    for (const word of config.data.replace) {
                        if (element.version == word.this) {
                            element.version = word.that
                            log += " with " + element.version
                            logger.info(log)
                        }
                    }
                }
            } catch { }




            if (element.name != lastname && lastname != "") {
                vernr++
            }

            lastname = element.name


            for (let i = 0; i < sources.length; i++) {
                for (let ii = 0; ii < sources[i].length; ii++) {
                    sources[i][ii] = sources[i][ii].replace('HEAD', element.version)
                }
            }


            if (!sources[vernr].includes(element.version)) {
                logger.warn("unequal version to branch name found in " + element.name + ": " + element.version + " != " + sources[vernr])
            }
        });
    })
}

function log_arr(data) {
    var out = "sources:"
    for (let i = 0; i < data.length; i++) {
        out += "\n" + i + ": "
        for (let ii = 0; ii < data[i].length; ii++)
            out += data[i][ii] + ", ";
    }
    logger.info(out)
}
